package View;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.event.WindowEvent;

import javax.swing.JTextField;

import java.awt.Font;

import javax.swing.SwingConstants;
import javax.swing.JTextPane;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.event.AncestorListener;
import javax.swing.event.AncestorEvent;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Component;

import javax.swing.Box;

import Controller.ControleSatelite;
import Model.*;

import java.awt.Dimension;


public class Interface extends JFrame {

	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel pnTable;
	private JPanel bAdd;
	private JScrollPane scrollTable;
	private JTable table;
	private JTable table_1;
	private JTable table_2;
	private JTable table_3;
	private JTable table_4;
	private JTable table_5;
	private Thread th = new Thread();
	private ControleSatelite controlSat = new ControleSatelite();
	private CorLed cores = new CorLed();
	private ImageandoGravando IG = new ImageandoGravando();
	private ImageandoTransmitindo IT = new ImageandoTransmitindo();
	private Standby standby = new Standby();
	
	

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface frame = new Interface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/* RELOGIO
	private static final long serialVersionUID = 1L;

	// Local onde atualizaremos a hora  
    private JLabel lblHora;  
  
    // Formatador da hora  
    private static final DateFormat FORMATO = new SimpleDateFormat("HH:mm:ss");  
  
    public void MyFrame() {  
        // Constru�mos nosso frame  
        
        setLayout(new FlowLayout());  
        getContentPane().add(getLblHora());  
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        setSize(200, 75);  
        setVisible(true);  
  
        // Iniciamos a thread do rel�gio. Tornei uma deamon thread para que seja  
        // automaticamente finalizada caso a aplica��o feche.  
        Thread clockThread = new Thread(new ClockRunnable(), "Clock thread");  
        clockThread.setDaemon(true);  
        clockThread.start();  
    }  
  
    private JLabel getLblHora() {  
        if (lblHora == null) {  
            lblHora = new JLabel();  
        }  
        return lblHora;  
    }  
  

    public void setHora(Date date) {  
        getLblHora().setText(FORMATO.format(date));  
    }  

    private class ClockRunnable implements Runnable {  
        public void run() {  
            try {  
                while (true) {  
                    // Aqui chamamos o setHora atrav�s da EventQueue da AWT.  
                    // Conforme dito, isso garante Thread safety para o Swing.  
                    EventQueue.invokeLater(new Runnable() {  
                        public void run() {  
                            // S� podemos chamar setHora diretamente dessa  
                            // forma, pois esse Runnable � uma InnerClass n�o  
                            // est�tica.  
                            setHora(new Date());  
                        }  
                    });  
                    // Fazemos nossa thread dormir por um segundo, liberando o  
                    // processador para outras threads processarem.  
                    Thread.sleep(1000);  
                }  
            }  
            catch (InterruptedException e) {  
            }  
        }  
    }  
  
    public static void main(String args[]) {  
        new MyFrame();  
    }  
}  */

	
	public Interface() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1224, 711);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(5, 5, 872, 344);
		contentPane.add(tabbedPane);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Menu Inicial", null, panel_2, null);
		panel_2.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("");
		
		lblNewLabel_1.setBounds(-135, 96, 1002, 172);
		Image img1 = new ImageIcon(this.getClass().getResource("/cbers.jpg")).getImage();
		lblNewLabel_1.setIcon(new ImageIcon(img1));
		panel_2.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Estação de Solo - CBERS 4");
		lblNewLabel_2.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 30));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(191, 18, 447, 40);
		panel_2.add(lblNewLabel_2);
		
		JLabel lblUmSoftwareDe = new JLabel("Um software de recebimento de dados e controle do satélite CBERS 4");
		lblUmSoftwareDe.setFont(new Font("Lucida Grande", Font.PLAIN, 14));
		lblUmSoftwareDe.setHorizontalAlignment(SwingConstants.CENTER);
		lblUmSoftwareDe.setBounds(126, 69, 588, 33);
		panel_2.add(lblUmSoftwareDe);
		
		JLabel lblDesenvolvidoPorDaniel = new JLabel("Desenvolvido por Daniel Moreira (daniel.s.br@ieee.org) e Letícia Munhoz (leticialmunhoz@gmail.com)");
		lblDesenvolvidoPorDaniel.setHorizontalAlignment(SwingConstants.CENTER);
		lblDesenvolvidoPorDaniel.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		lblDesenvolvidoPorDaniel.setBounds(166, 283, 520, 33);
		panel_2.add(lblDesenvolvidoPorDaniel);
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Exportar Dados", null, panel_3, null);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Dados Brutos", null, panel, null);
		
		table_5 = new JTable();
		panel.add(table_5);
		
		table_4 = new JTable();
		panel.add(table_4);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Histórico", null, panel_1, null);
		panel_1.setLayout(null);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBounds(10, 11, 847, 294);
		panel_1.add(panel_5);
		panel_5.setLayout(null);
		
		table_1 = new JTable();
		table_1.setBounds(10, 11, 827, 272);
		panel_5.add(table_1);
		
		table_2 = new JTable();
		table_2.setBounds(70, 53, 53, 75);
		panel_5.add(table_2);
		
		table_3 = new JTable();
		table_3.setBounds(-254, -69, 827, 272);
		panel_5.add(table_3);
		
		JLabel lblNewLabel = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("/mapa.jpg")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(17, 361, 851, 311);  
		contentPane.add(lblNewLabel);
		
		JLabel ledIT = new JLabel("•");
		ledIT.setHorizontalAlignment(SwingConstants.CENTER);
		ledIT.setFont(new Font("Lucida Grande", Font.PLAIN, 40));
		ledIT.setForeground(Color.green);
		ledIT.setBounds(1015, 210, 77, 40);
		contentPane.add(ledIT);
		
		JLabel ledStandby = new JLabel("•");
		ledStandby.setHorizontalAlignment(SwingConstants.CENTER);
		ledStandby.setForeground(Color.GREEN);
		ledStandby.setFont(new Font("Lucida Grande", Font.PLAIN, 40));
		ledStandby.setBounds(1015, 81, 77, 40);
		contentPane.add(ledStandby);
		
		final JLabel ledIG = new JLabel("•");
		ledIG.setHorizontalAlignment(SwingConstants.CENTER);
		ledIG.setFont(new Font("Lucida Grande", Font.PLAIN, 40));
		ledIG.setBounds(1014, 145, 77, 40);
		contentPane.add(ledIG);
		
		JLabel lblImageando = new JLabel("Imageando e \r\n\r\n\r\nTransmitindo");
		lblImageando.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblImageando.setBounds(986, 187, 149, 26);
		contentPane.add(lblImageando);
		
		JLabel lblImageandoEGravando = new JLabel("Imageando e Gravando");
		lblImageandoEGravando.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblImageandoEGravando.setBounds(987, 122, 129, 26);
		contentPane.add(lblImageandoEGravando);
		
		JLabel lblStandby = new JLabel("Standby");
		lblStandby.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblStandby.setBounds(1026, 61, 60, 26);
		contentPane.add(lblStandby);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBounds(887, 361, 311, 311);
		contentPane.add(panel_4);
		
		JLabel lblHorrioDeBraslia = new JLabel("Horário de Brasília");
		lblHorrioDeBraslia.setFont(new Font("Dialog", Font.PLAIN, 18));
		panel_4.add(lblHorrioDeBraslia);
		
		JLabel lblCmera = new JLabel("Câmera");
		lblCmera.setFont(new Font("Dialog", Font.PLAIN, 22));
		lblCmera.setBounds(1015, 24, 79, 26);
		contentPane.add(lblCmera);
		
		JLabel lblAmperagem = new JLabel("Erro");
		lblAmperagem.setFont(new Font("Dialog", Font.PLAIN, 22));
		lblAmperagem.setBounds(1036, 276, 56, 19);
		contentPane.add(lblAmperagem);
		
		JLabel ledErro = new JLabel("•");
		ledErro.setHorizontalAlignment(SwingConstants.CENTER);
		ledErro.setForeground(Color.GREEN);
		ledErro.setFont(new Font("Dialog", Font.PLAIN, 40));
		ledErro.setBounds(1015, 309, 77, 40);
		contentPane.add(ledErro);
		
			while(true) {
				cores = controlSat.setCor(IG);
				ledStandby.setForeground(cores.getCurrentColorSB());
				ledIT.setForeground(cores.getCurrentColorIT());
				ledIG.setForeground(cores.getCurrentColorIG());

				try {
					th.sleep(2592);
				} catch(Exception e) {}
				cores = controlSat.setCor(IT);
				ledStandby.setForeground(cores.getCurrentColorSB());
				ledIT.setForeground(cores.getCurrentColorIT());
				ledIG.setForeground(cores.getCurrentColorIG());
				try {
					th.sleep(1713);
				} catch(Exception e) {}
				cores = controlSat.setCor(IG);
				ledStandby.setForeground(cores.getCurrentColorSB());
				ledIT.setForeground(cores.getCurrentColorIT());
				ledIG.setForeground(cores.getCurrentColorIG());
				try {
					th.sleep(1552);
				} catch(Exception e) {}
				cores = controlSat.setCor(standby);
				ledStandby.setForeground(cores.getCurrentColorSB());
				ledIT.setForeground(cores.getCurrentColorIT());
				ledIG.setForeground(cores.getCurrentColorIG());
				try {
					th.sleep(5857);
				} catch(Exception e) {}
				
				int i = 0;
			
					while(i<13){
						cores = controlSat.setCor(IG);
						ledStandby.setForeground(cores.getCurrentColorSB());
						ledIT.setForeground(cores.getCurrentColorIT());
						ledIG.setForeground(cores.getCurrentColorIG());
						try {
							th.sleep(5857);
						} catch(Exception e) {}
						cores = controlSat.setCor(standby);
						ledStandby.setForeground(cores.getCurrentColorSB());
						ledIT.setForeground(cores.getCurrentColorIT());
						ledIG.setForeground(cores.getCurrentColorIG());
						try {
							th.sleep(5857);
						} catch(Exception e) {}
					}
			}
		
	/*	final int delay = 3280;
		while(true) {
			  final ActionListener taskPerformer1 = new ActionListener();
				  public void actionPerformed1(ActionEvent evt) {
		    	  	label_1.setForeground(Color.GREEN);
		    	  	new Timer(delay, taskPerformer1).start();
		      	}
		      final ActionListener taskPerformer2 = new ActionListener();
		    	  public void actionPerformed2(ActionEvent evt) {
		    		  label_1.setForeground(Color.RED);
		    		  new Timer(delay, taskPerformer2).start();
		    	  }
		};  */
		
		/*
		while(true) {
			label_1.setForeground(Color.GREEN);
			try {
				Thread.sleep(3280);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			label_1.setForeground(Color.GRAY);
			try {
				Thread.sleep(3280);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		*/
	}

	private void running() {
		// TODO Auto-generated method stub
		
	}
}