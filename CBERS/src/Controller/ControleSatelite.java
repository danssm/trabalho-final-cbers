package Controller;
import java.awt.Color;

import Model.CorLed;
import Model.Estados;



public class ControleSatelite {
	
/*
    1.7083 segundo de simula��o passando pela antena	
 */
	
	
/*
	14 Orbitas = novo tracking
	
	164 segundos = 23,934 (para poder passar diariamente pela mesma posi��o todos os dias do ano)
	
	Cada 11.7143 segundos de simula��o = picas led camera on/off
*/
	private Estados estado;
	private CorLed coresLed;
	
	
	public CorLed setCor(Estados estados){
					
		if(estados.isIluminado() == true && estados.isTorre() == true) {
			coresLed.setCurrentColorIT(Color.GREEN);
			coresLed.setCurrentColorIG(Color.GRAY);
			coresLed.setCurrentColorSB(Color.GRAY);
		}
		if(estados.isIluminado() == true && estados.isTorre() == false) {
			coresLed.setCurrentColorIT(Color.GRAY);
			coresLed.setCurrentColorIG(Color.GREEN);
			coresLed.setCurrentColorSB(Color.GRAY);
		}
		if(estados.isIluminado() == false && estados.isTorre() == false) {
			coresLed.setCurrentColorIT(Color.GRAY);
			coresLed.setCurrentColorIG(Color.GRAY);
			coresLed.setCurrentColorSB(Color.GREEN);
		}
		
		return coresLed;
	}
	
	
}
